#!/bin/bash

sed containers/Dockerfile* -ne 's/^FROM //p' | sort | uniq | xargs -n1 podman pull
